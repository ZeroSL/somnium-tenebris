﻿using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using UnityEngine.UI;
 public class AIController : MonoBehaviour
 {
 
     public Transform Player;
     int MoveSpeed = 3;
     int MaxDist = 10;
     float MinDist = 9.5f; 
     [SerializeField] private GameObject _theLight;
     private EncenderLinterna LinetrnaActiva;
     private bool _luzActiva;
 
     void Start()
     {
		     	LinetrnaActiva = _theLight.GetComponent<EncenderLinterna>();
		     	_luzActiva = LinetrnaActiva._turnOnOff;
 	 }
 
     void Update()
     {
     	_luzActiva = LinetrnaActiva._turnOnOff;
         transform.LookAt(Player);
 
         if (Vector3.Distance(transform.position, Player.position) <= MinDist && _luzActiva)
         {
             transform.position += transform.forward * MoveSpeed * Time.deltaTime;
 
 
             if (Vector3.Distance(transform.position, Player.position) <= MaxDist)
             {
                 //Here Call any function U want Like Shoot at here or something
             }
 
         }
     }
 }
 