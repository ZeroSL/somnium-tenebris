﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMasterEvents : MonoBehaviour {

	[SerializeField] GameObject _accessCard;
	public bool _canAccessOffice = false;

	[SerializeField] GameObject _switchCase;
	public bool _canUseElevator = false;


	// Update is called once per frame
	void Update () {
		CheckNullObjects ();
	}

	void CheckNullObjects()
	{
		if(_accessCard == null)
		{
			_canAccessOffice = true;
		}

		if(_switchCase == null)
		{
			_canUseElevator = true;
			Debug.Log (_canUseElevator);
		}
	}
}
