﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruirPorChoque : MonoBehaviour {

	private AudioSource _botellazo;
	private bool _controlSoundEffect = false;

	void Start()
	{
		_botellazo = this.gameObject.GetComponent<AudioSource> ();
	}

	void OnCollisionEnter(Collision collision)
	{
		if(collision.relativeVelocity.magnitude > 2 && !_controlSoundEffect)
		{
			_controlSoundEffect = true;
			_botellazo.Play ();
			GetComponent<MeshRenderer> ().enabled = false;
			Destroy(this.gameObject, 1f);
		}
	}
}
