﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpObject : MonoBehaviour {

	GameObject _mainCamera; //Camara principal
	GameObject _carriedObject; //Objeto tomado
	bool carrying; //Controlador de estado para tomar objetos
	public GameObject objcargado; //Posicion del handle para objeto
	private Pickupable p; //Acortamiento (refertencia) para objeto tomado
	private GameObject[] _pickupableObjects; //Array de objetos tomables
	public Image _puntero;

	// Use this for initialization
	void Start () {
		_puntero.CrossFadeAlpha (0.0f, 0.5f, false);
		_mainCamera = GameObject.FindWithTag("MainCamera");
		_pickupableObjects = GameObject.FindGameObjectsWithTag ("PickUpables");

	}
	
	// Update is called once per frame
	void Update () {
		if(carrying) {
			carry(_carriedObject);
			checkDrop();
			//rotateObject();
		} else {
			PickUp();
			GUIPointer ();
		}
	}

	void carry(GameObject o) {
		o.transform.position = objcargado.transform.position;
		o.transform.rotation = Quaternion.identity;
	}

	void PickUp(){
		if(Input.GetMouseButtonDown(0))
		{
			int _xScreen = Screen.width / 2;
			int _yScreen = Screen.height / 2;

			Ray ray = _mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(_xScreen,_yScreen));
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit))
			{
				p = hit.collider.GetComponent<Pickupable>();
				if (p != null && hit.distance <= 2.0f) 
				{
					if (p.gameObject.GetComponent<ItemToBag> () != null) {
						Destroy (p.gameObject);
					} else {
						carrying = true;
						_carriedObject = p.gameObject;
						p.gameObject.GetComponent<Rigidbody> ().useGravity = false;
					}
				}
			}
		}
	}

	void checkDrop() { //Funcion para checar si un objeto manejable puede arrojarse
		if(Input.GetMouseButtonDown(0)) { //Controlador de evento para arrojar  el objeto manejable en mano
			dropObject(); //Llamado de funcion para soltar el objeto manejable
		}
		else if(Input.GetMouseButtonDown(1))
		{
			dropsoftObject();
		}
	}

	void dropObject() { //Funcion soltar objeto manejable
		carrying = false; //Controlador de estado "cargando objeto"
		_carriedObject.gameObject.GetComponent<Rigidbody>().useGravity = true; //Obtención del componente rigidbody y activación de la gravedad del objeto
		_carriedObject.gameObject.GetComponent<Rigidbody> ().AddForce (transform.forward * 560f); //Adición de fuerza al objeto para arrojarlo hacia adelante
		_carriedObject.gameObject.GetComponent<Rigidbody> ().AddForce (transform.up * 300f); //Adición de fuerza al objeto para arrojarlo hacia arriba
		_carriedObject = null; //Objeto manejable en mano se convierte en nulo una vez arrojado
	}

	void dropsoftObject() { //Funcion soltar objeto manejable
		carrying = false; //Controlador de estado "cargando objeto"
		_carriedObject.gameObject.GetComponent<Rigidbody>().useGravity = true; //Obtención del componente rigidbody y activación de la gravedad del objeto
		_carriedObject = null; //Objeto manejable en mano se convierte en nulo una vez arrojado
	}

	/*NOTA: ESTA FUNCIÓN ES EXPERIMENTAL, 
	  PUEDE SER RETIRADA EN SI SE CONSIDERA NO OPTIMA PARA USO FUTURO 
	  U OPTIMIZADO DE LA VERSIÓN FINAL DEL PUNTERO
	  */
	void GUIPointer()
	{
		foreach(GameObject _pickIt in _pickupableObjects) //Llamado de cada objeto en el array de objetos para aquellos (objetos) que son manejables
		{
			if(_pickIt != null)
			{
				if (Vector3.Distance (this.gameObject.transform.transform.position, _pickIt.gameObject.transform.position) <= 2.0f) //Medida de la distancia de este objeto (player) con un objeto manejable
				{
					_puntero.CrossFadeAlpha (1.0f, 0.5f, false); //Visibilidad completa del puntero
				} 
				else 
				{
					_puntero.CrossFadeAlpha (0.0f, 0.5f, false); //Visibilidad nula del puntero
				}
			}
		}
	}
}
