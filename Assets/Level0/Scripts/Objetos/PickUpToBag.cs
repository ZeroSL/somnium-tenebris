﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpToBag : MonoBehaviour {

	GameObject _mainCamera; //Camara principal
	private ItemToBag _objectToBag; //Acortamiento (refertencia) para objeto tomado
	private GameObject[] _pickupableObjects; //Array de objetos tomables
	public Image _puntero;

	// Use this for initialization
	void Start () {
		_puntero.CrossFadeAlpha (0.0f, 0.5f, false);
		_mainCamera = GameObject.FindWithTag("MainCamera");
		_pickupableObjects = GameObject.FindGameObjectsWithTag ("ItemToBag");

	}

	// Update is called once per frame
	void Update () {
		
		PickUp();
		GUIPointer ();
	}
		
	void PickUp(){
		if(Input.GetMouseButtonDown(0))
		{
			int _xScreen = Screen.width / 2;
			int _yScreen = Screen.height / 2;

			Ray ray = _mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(_xScreen,_yScreen));
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit))
			{
				_objectToBag = hit.collider.GetComponent<ItemToBag>();
				if (_objectToBag != null && hit.distance <= 2.0f) 
				{
					Destroy (_objectToBag.gameObject);
				}
			}
		}
	}

	/*NOTA: ESTA FUNCIÓN ES EXPERIMENTAL, 
	  PUEDE SER RETIRADA EN SI SE CONSIDERA NO OPTIMA PARA USO FUTURO 
	  U OPTIMIZADO DE LA VERSIÓN FINAL DEL PUNTERO
	  */
	void GUIPointer()
	{
		foreach(GameObject _pickIt in _pickupableObjects) //Llamado de cada objeto en el array de objetos para aquellos (objetos) que son manejables
		{
			if (_pickIt != null) { 
				if (Vector3.Distance (this.gameObject.transform.transform.position, _pickIt.gameObject.transform.position) <= 2.0f) { //Medida de la distancia de este objeto (player) con un objeto manejable
					_puntero.CrossFadeAlpha (1.0f, 0.5f, false); //Visibilidad completa del puntero
				} else {
					_puntero.CrossFadeAlpha (0.0f, 0.5f, false); //Visibilidad nula del puntero
				}
			}
		}
	}
}
