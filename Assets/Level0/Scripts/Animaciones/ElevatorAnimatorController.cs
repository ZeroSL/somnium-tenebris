﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorAnimatorController : MonoBehaviour {

	[SerializeField] GameObject ElevatorDoors;
	private Animator _animator;
	private bool _toggleAnimation;

	[SerializeField] GameObject _gameMasterEvents;
	private GameMasterEvents _controlDeGameMaster;

	// Use this for initialization
	void Start () {
		_animator = ElevatorDoors.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		_toggleAnimation = _animator.GetBool ("IsOpened");
	}

	void OnTriggerStay(Collider collider){
		_controlDeGameMaster = _gameMasterEvents.gameObject.GetComponent<GameMasterEvents> ();
		if(_controlDeGameMaster._canUseElevator)
		{
			if(Input.GetMouseButtonDown(0) && !_toggleAnimation) { //Controlador de evento para arrojar  el objeto manejable en mano
				_animator.SetBool ("IsOpened", true);
			}else if(Input.GetMouseButtonDown(0) && _toggleAnimation) 
			{ 
				_animator.SetBool ("IsOpened", false);
			}
		}
	}
}
