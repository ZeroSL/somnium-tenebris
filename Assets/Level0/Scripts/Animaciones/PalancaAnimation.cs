﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalancaAnimation : MonoBehaviour {

	[SerializeField] GameObject SwitchCase;
	private Animator _animator;

	[SerializeField] GameObject _activadorSwitch;

	void Start()
	{
		_animator = SwitchCase.GetComponent<Animator> ();	
	}

	void OnTriggerStay(Collider collider)
	{
		if(Input.GetMouseButtonDown(0)) { //Controlador de evento para arrojar  el objeto manejable en mano
			_animator.SetBool("TurnSwitch", true);
			Destroy (_activadorSwitch, 1.5f);
		}
	}
}
