﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirPuertaOficina : MonoBehaviour {

	[SerializeField] GameObject _puertaOficina;

	[SerializeField] GameObject _gameMasterEvents;
	private GameMasterEvents _controlDeGameMaster;

	void OnTriggerStay(Collider collider)
	{
		_controlDeGameMaster = _gameMasterEvents.gameObject.GetComponent<GameMasterEvents> ();
		if(_controlDeGameMaster._canAccessOffice)
		{
			if(Input.GetMouseButtonDown(0)) { //Controlador de evento para arrojar  el objeto manejable en mano
				Destroy(_puertaOficina);
			}
		}
	}
}
